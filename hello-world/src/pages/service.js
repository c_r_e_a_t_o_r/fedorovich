import React from "react"
import Layouts from "../component/layouts"

export default () => (
    
    <div>
       <Layouts> <div className="about-banner">
    <div className="container text-center py-5 text-white">
      <h1>About Our Company</h1>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. <br/> Sapiente, tempore?</p>
    </div>
  </div>
  
  
  <div className="servic my-5">
    <div className="container text-center">
      <div className="row">
        <div className="col-md-4">
          <div className="card">
            <div className="card-header bg-dark">
              <h3>Services plan one</h3>
            </div>
            <div className="card-bod">
              <h4>$49.99/month</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt, distinctio?</p>
              <ul className="list-group">
                <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature one
                </li>
                <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature two
                </li>
                 <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature three
                </li>
                 <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature four
                </li>
                 <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature five
                </li>
              </ul>
  
              <button className="btn btn-danger mt-2 btn-block">Get It</button>
           
            </div>
            <div className="card-footer">
              <span className="text-muted"> 1 Year Plan</span>
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <div className="card">
            <div className="card-header bg-dark">
              <h3>Services plan one</h3>
            </div>
            <div className="card-bod">
              <h4>$49.99/month</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt, distinctio?</p>
              <ul className="list-group">
                <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature one
                </li>
                <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature two
                </li>
                 <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature three
                </li>
                 <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature four
                </li>
                 <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature five
                </li>
              </ul>
  
              <button className="btn btn-danger mt-2 btn-block">Get It</button>
           
            </div>
            <div className="card-footer">
              <span className="text-muted"> 1 Year Plan</span>
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <div className="card">
            <div className="card-header bg-dark">
              <h3>Services plan one</h3>
            </div>
            <div className="card-bod">
              <h4>$49.99/month</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt, distinctio?</p>
              <ul className="list-group">
                <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature one
                </li>
                <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature two
                </li>
                 <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature three
                </li>
                 <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature four
                </li>
                 <li className="list-group-item">
                  <i className="fas fa-check"></i>
                  Feature five
                </li>
              </ul>
  
              <button className="btn btn-danger mt-2 btn-block">Get It</button>
           
            </div>
            <div className="card-footer">
              <span className="text-muted"> 1 Year Plan</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div></Layouts>
    </div>

)