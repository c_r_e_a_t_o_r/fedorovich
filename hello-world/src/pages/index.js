import React from "react"
import Layouts from "../component/layouts"
import Slider from "../component/slider"
import Container from "../component/container"


export default () => (
    
    <div>
        <Layouts>
        <Slider />
        <Container />
        </Layouts>
    </div>

)