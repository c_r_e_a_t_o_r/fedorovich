import React from "react"
import Layouts from "../component/layouts"

export default () => (
    
    <div>
        <Layouts>

        <div className="about-banner">
  <div className="container text-center py-5 text-white">
    <h1>About Our Company</h1>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. <br/> Sapiente, tempore?</p>
  </div>
</div>

 <div className="about-content">
   <div className="container">
   <div className="row py-5">
    <div className="col-md-6">
      <h3>What we Do?</h3>
      <p> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quis, voluptate!Lorem ipsum dolor sit amet consectetur adipisicing elit. , accusamus doloremque quos sit, nulla iste aliquid alias minus voluptatibus dignissimos quo saepe eveniet labore officiis officia ducimus. Sequi nostrum explicabo quam doloribus placeat voluptatem? Fugiat quaerat deserunt sequi.</p>
      <p> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nam, quae? Lorem, ipsum dolor sit amet consectetur adipisicing elit nobis similique dolores veniam nulla pariatur nesciunt, earum accusamus consectetur magnam sit corrupti sint, ex ad non soluta facere quaerat! Laudantium eius tempore perferendis quo explicabo facere iste.</p>
    </div>
    <div className="col-md-6">
      <img className=" about-img img-fluid rounded-circle" src="./img/carousel2.jpg" alt="" />
    </div>
   </div>
   </div>
 </div>
   <div className="about-icon">
     <div className="container text-white text-center">
       <div className="row">
         <div className="col-md-4">
           <div className="card bg-danger p-4">
             <i className="fas fa-building fa-3x"></i>
             <h3>Sample Heading</h3>
             <p className="m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem sint quasi officia repellat veritatis eos.</p>
           </div>
          </div>
          <div className="col-md-4">
            <div className="card bg-dark p-4">
              <i className="fas fa-bullhorn fa-3x"></i>
              <h3>Sample Heading</h3>
              <p className="m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem sint quasi officia repellat veritatis eos.</p>
            </div>
           </div>
           <div className="col-md-4">
            <div className="card bg-dark p-4">
              <i className="fas fa-comments fa-3x"></i>
              <h3>Sample Heading</h3>
              <p className="m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem sint quasi officia repellat veritatis eos.</p>
            </div>
           </div>

           <div className="row mt-4">
            <div className="col-md-4">
              <div className="card bg-danger p-4">
                <i className="fas fa-box fa-3x"></i>
                <h3>Sample Heading</h3>
                <p className="m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem sint quasi officia repellat veritatis eos.</p>
              </div>
             </div><div className="col-md-4">
              <div className="card bg-dark p-4">
                <i className="fas fa-credit-card fa-3x"></i>
                <h3>Sample Heading</h3>
                <p className="m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem sint quasi officia repellat veritatis eos.</p>
              </div>
             </div><div className="col-md-4">
              <div className="card bg-danger p-4">
                <i className="fas fa-coffee fa-3x"></i>
                <h3>Sample Heading</h3>
                <p className="m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem sint quasi officia repellat veritatis eos.</p>
              </div>
             </div>
           </div>

       </div>
     </div>
   </div>
        </Layouts>
    </div>

)