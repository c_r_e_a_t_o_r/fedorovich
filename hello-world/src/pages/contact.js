import React from "react"
import Layouts from "../component/layouts"

export default () => (
    
<div>
    <Layouts>
        <div className="about-banner">
            <div className="container text-center py-5 text-white">
                <h1>About Our Company</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. <br/> Sapiente, tempore?</p>
            </div>
        </div>

  <div className="contact-info bg-dark text-white">
    <div className="row">
      <div className="col-md-4 offset-md-2">
        <div className="icon-boxes my-5 d-flex">
          <i className="fas fa-truck fa-2x bg-danger p-3 mr-3"></i>
          <h3>Trucking Shipping <br/> Services</h3>
        </div>
        <div className="icon-boxes my-5 d-flex">
          <i className="fas fa-truck fa-2x bg-danger p-3 mr-3"></i>
          <h3>Trucking Shipping <br/> Services</h3>
        </div>
        <div className="icon-boxes my-5 d-flex">
          <i className="fas fa-truck fa-2x bg-danger p-3 mr-3"></i>
          <h3>Trucking Shipping <br/> Services</h3>
        </div>
      </div>

      <div className="col-md-6"><img src="img/media.jpg" alt="" className="img-fluid"/></div>
    </div>
  </div>


  <div className="contact-form my-5">
    <div className="container">
      <div className="row">
        <div className="col-md-4">
          <div className="card p-3">
            <div className="card-body">
              <h4>Get In Touch</h4>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
              <h4>Address</h4>
              <p>123 Main St</p>
              <h4>Email</h4>
              <p>Lorem, ipsum.</p>
              <h4>Phone</h4>
              <p>+99897 434 4332</p>
            </div>
          </div>
        </div>


        <div className="col-md-8">
          <div className="card p-3">
            <div className="card-body">
              <h3 className="text-center">Please fill out the form to contact us</h3>
              

              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <input type="text" className="form-control"/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <input type="text" className="form-control"/>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <input type="text" className="form-control"/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <input type="text" className="form-control"/>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-group">
                    <textarea className="form-control"></textarea>
                  </div>
                </div>
              </div>

                     <div className="row">
                        <div className="col">
                        <button className="btn btn-danger btn-block">submit</button>
                        </div>
                        </div>
                        </div>
                        </div>

                </div>
            </div>
         </div>
        </div>
</Layouts>

</div>
)