import React from "react"
import{Link} from "gatsby"


export default () =>

<div>
<nav className="navbar navbar-expand-sm navbar-dark bg-own">
    <div className="container">
      <Link to="/" className="navbar-brand">Federovich</Link>
      <button className="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarCollapse">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item active">
            <Link to="/" className="nav-link">Home</Link>
          </li>
          <li className="nav-item">
            <Link to="/about" className="nav-link">About Us</Link>
          </li>
          <li className="nav-item">
            <Link to="/service" className="nav-link">Service</Link>
          </li>
          <li className="nav-item">
            <Link to="/blog" className="nav-link">Blog</Link>
          </li>
          <li className="nav-item">
            <Link to="/contact" className="nav-link">Contact"</Link>
          </li>
        </ul>
      </div>
    </div>
  </nav>

</div>