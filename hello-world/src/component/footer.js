import React from "react"



export default () =>


<footer id="main-footer" className="text-center text-white py-5">
    <div className="container">
      <div className="row">
        <div className="col">
          <p>Copyright &copy;
            <span id="year"></span> Glozzom</p>
        </div>
      </div>
    </div>
  </footer>
