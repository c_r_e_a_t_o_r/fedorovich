import React from "react"



export default () =>

<div>
<div className="container">
    <div className="row mt-5">
      <div className="col-md-6">

          <img className="img-fluid" src="https://static.wixstatic.com/media/63b81c_c66398e9ec36463a9b62601e6cd020d7.jpg/v1/fill/w_562,h_375,al_c,q_80,usm_0.66_1.00_0.01/63b81c_c66398e9ec36463a9b62601e6cd020d7.jpg" alt=""/>>
      </div>
       <div className="col-md-6">
          <img className="img-fluid" src="https://static.wixstatic.com/media/63b81c_c66398e9ec36463a9b62601e6cd020d7.jpg/v1/fill/w_562,h_375,al_c,q_80,usm_0.66_1.00_0.01/63b81c_c66398e9ec36463a9b62601e6cd020d7.jpg" alt=""/>>
      </div>
    </div>
  </div>


      <div className="container">
        <h3 className="mt-4">
          Lorem ipsum dolor sit.
        </h3>
        <div className="row mt-5">
          <div className="col-md-4">
             <img className="img-fluid" src="https://static.wixstatic.com/media/63b81c_c66398e9ec36463a9b62601e6cd020d7.jpg/v1/fill/w_562,h_375,al_c,q_80,usm_0.66_1.00_0.01/63b81c_c66398e9ec36463a9b62601e6cd020d7.jpg" alt=""/>>
          </div>
          <div className="col-md-8 mt-2">
            <h5>  Lorem ipsum dolor sit amet.</h5>
            <p>   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam sequi corporis eos recusandae error placeat totam minima, saepe, adipisci. Adipisci qui illum quaerat, beatae ipsam quasi, laborum placeat debitis soluta.</p>
            <p>   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, libero natus nesciunt temporibus obcaecati possimus quaerat ullam maiores velit. Animi voluptatum harum doloribus eos consequatur ratione unde! Fuga, quam, eius.</p>
          </div>
        </div>
      </div>
      <div className="container">  
         <div className="row mt-5 py-4"> 
          <div className="col-md-3"> 
            <h5> Lorem ipsum.</h5>
              <p className="m-2">   Lorem ipsum <br/>   dolor sit amet.</p>
            </div>
               <div className="col-md-3"> 
                <h5> Lorem ipsum.</h5>
                <p className="m-2">   Lorem ipsum <br/>   dolor sit amet.</p>
            </div>   <div className="col-md-3"> 
                <h5> Lorem ipsum.</h5>
                <p className="m-2">   Lorem ipsum <br/>   dolor sit amet.</p>
            </div>   <div className="col-md-3"> 
                <h5> Lorem ipsum.</h5>
                <p className="m-2">   Lorem ipsum <br/>   dolor sit amet.</p>
            </div>
        </div>
      </div>
      <div id="newsletter" className="py-4 my-5 bg-dark text-center text-white" >
       <div className="container">
         <h1>Sign Up to our newsletter</h1>
         <p>Lorem lorem ipsum dolor sit. ipsum dolor sit amet consectetur adipisicing elit. Nam, est.</p>
          <form className="form-inline justify-content-center">
            <input className=" mr-2 form-control" type="text" placeholder="Your name" />
            <input className=" mr-2 form-control" type="email" placeholder="Your email" />
            <button className="btn btn-primary">SIGN UP</button>
          </form>
        </div>
     </div>


</div>